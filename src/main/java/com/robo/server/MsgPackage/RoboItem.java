package com.robo.server.MsgPackage;

/**
 * Created by yoki on 6/27/17.
 */
public class RoboItem {


    public String robotId;

    public RoboConnectStatus status; // specific to msg type.

    public String hostName;

    public RoboConnectStatus getStatus() {
        return status;
    }

    public void setStatus(RoboConnectStatus status) {
        this.status = status;
    }



    public String getRobotId() {
        return robotId;
    }

    public void setRobotId(String robotId) {
        this.robotId = robotId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }



}
