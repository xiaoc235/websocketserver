package com.robo.server.MsgPackage;

import com.common.base.response.BaseResponseDto;
import com.common.utils.GsonUtils;
import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by yoki on 6/26/17.
 */
public class MsgInitConnect extends Handler {


    private  RoboItem item;

    public MsgInitConnect() {
        item = new RoboItem();
    }
    @Override
    public BaseResponseDto handle(Message message) {

        item.setStatus(RoboConnectStatus.INIT_CONNECT);
        LinkedTreeMap<String, String> obj =  (LinkedTreeMap< String, String>)message.getData();
        item.setRobotId(obj.get("id"));
        item.setHostName(obj.get("hostname"));

        BaseResponseDto responseDto = new BaseResponseDto();
        responseDto.setCode(0);
        responseDto.setSuccess(true);
        responseDto.setMessage("succeed: init connection");
        DataStartRobot dataStart = new DataStartRobot();
        dataStart.setVal("id1","service1");
        ResponseInfo data = new ResponseInfo();
        data.setType(RoboConnectStatus.START.val() );
        data.setData(dataStart );
        responseDto.setData(data);
        return responseDto;
    }

    @Override
    public RoboItem getItem() {
        return  item;
    }
}
