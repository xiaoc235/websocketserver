package com.robo.server.MsgPackage;

import com.robo.server.web.em.websocket.WebsocketMethodEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yoki on 6/28/17.
 */
public enum RoboConnectStatus {
    INIT_CONNECT (1),
    START (2),
    QUIT(3),
    KICK (4),
    CONFIG(5),
    CONFIG_QUESTION (6),
    SWITCHON(7),
    SENDUUID(8),
    LOGIN(9);

     int constant;
     private RoboConnectStatus(int i) {
         constant = i;
     }

    public int val() {
        return this.constant;
    }

    /**
     * 映射
     */
    private static Map<String, Integer> desc = new HashMap<>();
    static {
        desc.put(WebsocketMethodEnum.sendUUId.getValue(),SENDUUID.val());
        desc.put(WebsocketMethodEnum.userLogin.getValue(),LOGIN.val());
        desc.put(WebsocketMethodEnum.updateSetting.getValue(),CONFIG.val());
    }
    public static Integer getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return null;
        }
    }

}
