package com.robo.server.MsgPackage;

/**
 * Created by yoki on 6/27/17.
 */
public class Message {
    /**
     * type:
     *    = 0 , reserved.
     *    = 1 , init the connection
     *    = 2 , start the robot
     *    = 3 , quit robot
     *    = 4 , refresh config file
     *    = 5 , refresh question list
     *    = 6 , switch robot on/off the auto reply status
     */
    public int type;
    public Object data;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
