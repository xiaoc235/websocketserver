package com.robo.server.MsgPackage;

import com.common.base.response.BaseResponseDto;
import com.common.utils.GsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Null;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yoki on 6/27/17.
 */
public class MsgHandler {
    private static final Logger _logger = LoggerFactory.getLogger(MsgHandler.class);
    private Map<Session, RoboItem> _mapSessionItems ;

    public MsgHandler() {
        _mapSessionItems = new HashMap<>();
    }


    public void refreshSession(Session session) {
       RoboItem item = _mapSessionItems.get(session);
       if(item != null)
         _logger.info("close the session of robot:" + item.getRobotId());
        _mapSessionItems.remove(session);
    }

    public void addSession(Session session) {
        _logger.info(" adding a new user" + session.toString());
        RoboItem item = new RoboItem();

        _mapSessionItems.put(session, item);
    }







    /**
     * handle message from client
     * @param message
     * @return
     */
    public BaseResponseDto handle(Session session, String message) {
        _logger.info("handle: " + message);
        BaseResponseDto responseDto = new BaseResponseDto();
        Message  msg = GsonUtils.convertObj(message, Message.class);
        RoboItem item  = _mapSessionItems.get(session);
        Handler handler  = null;
        switch(msg.getType()) {
            case 0:
                break;
            case 1:
            {
                // response on init the connction
                handler = new MsgInitConnect();
                break;
            }
            case 2:
            {
                // response on start the robot
                handler = new MsgStartRobot();
                item.setStatus(RoboConnectStatus.START);
                break;
            }
            case 3:
            {
                // response on quit the robot
                handler = new MsgQuitRobot();
                item.setStatus(RoboConnectStatus.QUIT);
                break;
            }
            case 4:
            {
                //response on configuration file refresh
                handler = new  MsgRefreshConfig();
                item.setStatus(RoboConnectStatus.CONFIG);
                break;
            }
            case 5:
            {
                // response on configuration question list refresh
                handler = new MsgRefreshQuestionList();
                item.setStatus(RoboConnectStatus.CONFIG_QUESTION);
                break;
            }
            case 6:
            {
                // response on auto-reply switch-on/off
                handler = new MsgSwitchAutoHandle();
                item.setStatus(RoboConnectStatus.SWITCHON);
                break;
            }
        }

        responseDto = handler.handle(msg);
        item =  handler.getItem();
        _mapSessionItems.put(session, item);

        return responseDto;
    }


    public void sendMessage(String message, Session session) throws IOException {
        _logger.info("sendMessage  : " + message);
        session.getBasicRemote().sendText(message);
    }


}
