package com.robo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@ComponentScan({"com.robo.server.websocket","com.robo.server.controller","com.common.*"})
@SpringBootApplication
public class WebSocketApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(WebSocketApplication.class, args);
	}


	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(WebSocketApplication.class);
	}

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}


}
