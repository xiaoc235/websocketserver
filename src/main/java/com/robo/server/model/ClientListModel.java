package com.robo.server.model;

import com.common.base.BaseDto;

/**
 * 客户端列表
 * Created by jianghaoming on 17/8/9.
 */
public class ClientListModel extends BaseDto {

    private String clientId; //ws客户端id

    private String roboId; //对应的机器人id

    private String userId; //对应的userId

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRoboId() {
        return roboId;
    }

    public void setRoboId(String roboId) {
        this.roboId = roboId;
    }
}
