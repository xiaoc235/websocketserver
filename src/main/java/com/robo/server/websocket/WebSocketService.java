package com.robo.server.websocket;


import com.common.base.CommConstants;
import com.common.base.response.BaseResponseDto;
import com.common.redis.RedisClient;
import com.common.redis.RedisManager;
import com.robo.server.model.ClientListModel;
import com.robo.server.web.em.cache.CacheWebScoketEnum;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jianghaoming on 17/6/15.
 */
public class WebSocketService {

    private RedisClient redisClient;

    protected RedisClient getRedis(){
        if(redisClient == null){
            redisClient = RedisManager.getRedis();
        }
        return redisClient;
    }

    protected static final String cache_client_list = CacheWebScoketEnum.client_lsit.getValue();





    /**
     * ws客户端在线列表 管理
     * @return
     */
    public List<ClientListModel> getClientList(){
        List<ClientListModel> list = getRedis().getList(cache_client_list);
        if(list == null){
            list = new ArrayList<>();
        }
        return list;
    }

    public void setClientList(List<ClientListModel> list){
        getRedis().set(cache_client_list,list);
    }



    /**
     * 返回请求失败
     * @return
     */
    protected static BaseResponseDto failResponse(String message){
        return new BaseResponseDto(false, HttpStatus.OK.value(), message, new HashMap<>());
    }

    /**
     * 返回成功消息
     * @param message
     * @return
     */
    protected static BaseResponseDto succResponse(String message, Object obj){
        return new BaseResponseDto(true, HttpStatus.OK.value(),message,obj);
    }

    protected static BaseResponseDto succResponse(String message){
        return new BaseResponseDto(true, HttpStatus.OK.value(),message, new HashMap<>());
    }



}
